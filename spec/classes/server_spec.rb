# frozen_string_literal: true

require 'spec_helper'

describe 'borgbackup::server' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:params) do
        {
          'clients' => {
            'host1' => {
              'type' => 'ssh-rsa',
              # rubocop:disable Layout/LineLength
              'data' => 'AAAAB3NzaC1yc2EAAAADAQABAAABgQDLmzwL6NOwrY2XcWDOsdXJ/0q6cdgZZT7DZ3CimpeOhSrpJjMu/fY8zVcGrtYnJf2BjijeHqapRUKMjUbhE9mSa8KPnt96mJK+K+oJtwGylzD7pFcCCbddZJxzzcgTCYB67usmpPcZNP1K+K9tNracV4XwneUT2A4xo3y3US1i8Wal5eA//BFX5aH5C24LftuKJfi/33+s8GBR9iGnwTx94qTY4p2nAIGqM2G3DNXswf9/4A5ipf4VTV1+IwF+s/PJpCN2n9UVfg9Lr9MWKK0ztuDepPXW4tzotlX06wypZlOq2QeIGcs0i26dKmJb8szbgAqF0QeXdMQaiRpNShmQjNHU47t35ErAQAq5kqvuR9T4wfrE7H/zhbfosukzjhkhoKhnNp2igsJhJjmBefcfmUO1MyQqMZ2rZROs4BytBdmSTams84D8+a7iZaiMJwgRoyP+zsONF0sO8gnxzamNp9OmYStpURfo2UaITbgkuAdA6edwGMdVZF4Ila4P5+s=',
              # rubocop:enable Layout/LineLength
            },
          },
        }
      end

      it { is_expected.to compile.with_all_deps }
    end
  end
end
