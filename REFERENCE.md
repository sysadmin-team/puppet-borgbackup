# Reference

<!-- DO NOT EDIT: This document was generated by Puppet Strings -->

## Table of Contents

### Classes

* [`borgbackup`](#borgbackup): Installs borgbackup
* [`borgbackup::client`](#borgbackupclient): Configures a backup client
* [`borgbackup::server`](#borgbackupserver): Configures a backup server

### Defined types

* [`borgbackup::backup_fs`](#borgbackupbackup_fs): Sets up a weekly backup job for a filesystem
* [`borgbackup::backup_lv`](#borgbackupbackup_lv): Sets up a weekly backup job for a logical volume

## Classes

### `borgbackup`

This class installs borgbackup.

#### Examples

##### 

```puppet
include borgbackup
```

### `borgbackup::client`

This class deploys the required scripts and dependencies to run backup
jobs on a node.

#### Examples

##### 

```puppet
class { 'borgbackup::client':
  borgpassphrase => 'supersecret',
}
```

#### Parameters

The following parameters are available in the `borgbackup::client` class.

##### `borgpassphrase`

Data type: `String`

The BORG_PASSPHRASE environment variable (for more info, see:
https://borgbackup.readthedocs.io/en/stable/usage/general.html)

### `borgbackup::server`

This class ensures borgbackup is installed and that for every client:
  - there is a user, named after the client, with a homedirectory
  - the client has ssh access to write backups append-only

#### Examples

##### 

```puppet
class { 'borgbackup::server':
  clients => {
    host1 => {
      type => 'ssh-rsa',
      data => 'AAAAB3NzaC1yc2EAAA...',
    },
  },
}
```

#### Parameters

The following parameters are available in the `borgbackup::server` class.

##### `clients`

Data type: `Hash`

A hash containing all the names of all the nodes that need to make
backups and their respective root users' ssh public key.

## Defined types

### `borgbackup::backup_fs`

Creates a systemd timer that calls the backup script for backing up
a filesystem. Defaults to backing up everything, this can be adjusted
by setting the mountpoint parameter.

#### Examples

##### 

```puppet
borgbackup::backup_fs { 'backstuffup':
  excludes     => ['/var/log','/tmp'],
  backupserver => 'backups.example.org',
}
```

#### Parameters

The following parameters are available in the `borgbackup::backup_fs` defined type.

##### `backupserver`

Data type: `String`

The server backups are written to.

##### `mountpoint`

Data type: `String`

The point in our filesystem from where we want to back up, defaults
to / to provide full backups.

Default value: `'/'`

##### `backupuser`

Data type: `String`

The username on the backupserver, defaults to the client's hostname.

Default value: `$trusted['hostname']`

##### `basedir`

Data type: `String`

The directory on the backupserver in which our borg repository
resides.

Default value: `"/home/${backupuser}"`

##### `repo`

Data type: `String`

The name of our borg repository.

Default value: `$title`

##### `dow`

Data type: `String`

The day of the week in which we want the backup job to run.

Default value: `'Sun'`

##### `rnd`

Data type: `String`

The random time offset (from midnight) on which the backup job is
run.

Default value: `'24h'`

##### `excludes`

Data type: `Array[String]`

An array of patterns for paths to exclude from backups (see:
https://borgbackup.readthedocs.io/en/stable/usage/create.html)

Default value: `[]`

### `borgbackup::backup_lv`

Creates a systemd timer that calls the backup script for backing up
logical volumes. The volume can contain either a partitioned disk
image or a filesystem. In the latter case, rawdisk should be set to
true.

#### Examples

##### 

```puppet
borgbackup::backup_lv { 'backstuffup':
  excludes     => ['/var/log','/tmp'],
  backupserver => 'backups.example.org',
  vg           => 'myvolume',
  lv           => 'stuff',
}
```

#### Parameters

The following parameters are available in the `borgbackup::backup_lv` defined type.

##### `backupserver`

Data type: `String`

The server backups are written to.

##### `vg`

Data type: `String`

The volume group in which the logical volume to back up resides.

##### `lv`

Data type: `String`

The logical volume to back up.

##### `backupuser`

Data type: `String`

The username on the backupserver, defaults to the client's hostname.

Default value: `$trusted['hostname']`

##### `basedir`

Data type: `String`

The directory on the backupserver in which our borg repository
resides.

Default value: `"/home/${backupuser}"`

##### `repo`

Data type: `String`

The name of our borg repository.

Default value: `$title`

##### `dow`

Data type: `String`

The day of the week in which we want the backup job to run.

Default value: `'Sun'`

##### `rnd`

Data type: `String`

The random time offset (from midnight) on which the backup job is
run.

Default value: `'24h'`

##### `excludes`

Data type: `Array[String]`

An array of patterns for paths to exclude from backups (see:
https://borgbackup.readthedocs.io/en/stable/usage/create.html)

Default value: `[]`

##### `rawdisk`

Data type: `Boolean`

A boolean describing whether the logical volume directly contains
a filesystem (as opposed to a partitioned disk image).

Default value: ``false``

