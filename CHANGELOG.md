# Changelog

All notable changes to this project will be documented in this file.

## Release 0.1.2

**Bugfixes**

Fixes typo in systemd service.

## Release 0.1.1

**Features**

Added a parameter to set the randomised delay at which a backup job runs and 
changed the default from 4 to 24 hours to remove bias towards particular
timezones.

## Release 0.1.0

**Features**

Weekly backups of logical volumes and/or filesystem

**Bugfixes**

none

**Known Issues**

none
