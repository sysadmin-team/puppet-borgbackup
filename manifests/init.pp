# @summary Installs borgbackup
#
# This class installs borgbackup.
#
# @example
#   include borgbackup
class borgbackup {
  ensure_packages(['borgbackup'])
}
