# @summary Sets up a weekly backup job for a logical volume
#
# Creates a systemd timer that calls the backup script for backing up
# logical volumes. The volume can contain either a partitioned disk
# image or a filesystem. In the latter case, rawdisk should be set to
# true.
#
# @example
#   borgbackup::backup_lv { 'backstuffup':
#     excludes     => ['/var/log','/tmp'],
#     backupserver => 'backups.example.org',
#     vg           => 'myvolume',
#     lv           => 'stuff',
#   }
#
# @param backupserver
#   The server backups are written to.
#
# @param vg
#   The volume group in which the logical volume to back up resides.
#
# @param lv
#   The logical volume to back up.
#
# @param backupuser
#   The username on the backupserver, defaults to the client's hostname.
#
# @param basedir
#   The directory on the backupserver in which our borg repository
#   resides.
#
# @param repo
#   The name of our borg repository.
#
# @param dow
#   The day of the week in which we want the backup job to run.
#
# @param rnd
#   The random time offset (from midnight) on which the backup job is
#   run.
#
# @param excludes
#   An array of patterns for paths to exclude from backups (see:
#   https://borgbackup.readthedocs.io/en/stable/usage/create.html)
#
# @param rawdisk
#   A boolean describing whether the logical volume directly contains
#   a filesystem (as opposed to a partitioned disk image).
#
define borgbackup::backup_lv (
  String $backupserver,
  String $vg,
  String $lv,
  String $backupuser      = $trusted['hostname'],
  String $basedir         = "/home/${backupuser}",
  String $repo            = $title,
  String $dow             = 'Sun',
  String $rnd             = '24h',
  Array[String] $excludes = [],
  Boolean $rawdisk        = false,
) {
  include borgbackup::client

  file { "/etc/borgbackup/${repo}.excludes":
    content => epp('borgbackup/excludes.epp', { excludes => $excludes }),
    mode    => '0600',
    owner   => 'root',
    group   => 'root',
  }

  systemd::timer { "run-backup-${repo}.timer":
    timer_content   => epp('borgbackup/run-backup.timer.epp', {
        dow => $dow,
        rnd => $rnd,
    }),
    service_content => epp('borgbackup/run-backup-lv.service.epp', {
        vg           => $vg,
        lv           => $lv,
        backupuser   => $backupuser,
        backupserver => $backupserver,
        basedir      => $basedir,
        repo         => $repo,
        rawdisk      => $rawdisk,
    }),
    active          => true,
    enable          => true,
    require         => File["/etc/borgbackup/${repo}.excludes"],
  }

  exec { "/usr/bin/borg init --encryption=keyfile --append-only 'ssh://${backupuser}@${backupserver}/${basedir}/${repo}'":
    onlyif      => "/usr/bin/borg list 'ssh://${backupuser}@${backupserver}/${basedir}/${repo}' 2>&1 |grep -qs 'does not exist'",
    environment => "BORG_PASSPHRASE=${borgbackup::client::borgpassphrase}",
    require     => Class['borgbackup::client'],
  }
}
