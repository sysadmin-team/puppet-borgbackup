# @summary Configures a backup server
#
# This class ensures borgbackup is installed and that for every client:
#   - there is a user, named after the client, with a homedirectory
#   - the client has ssh access to write backups append-only
#
# @example
#   class { 'borgbackup::server':
#     clients => {
#       host1 => {
#         type => 'ssh-rsa',
#         data => 'AAAAB3NzaC1yc2EAAA...',
#       },
#     },
#   }
#
# @param clients
#   A hash containing all the names of all the nodes that need to make
#   backups and their respective root users' ssh public key.
#
class borgbackup::server (
  Hash $clients,
) {
  include borgbackup

  $clients.each |String $client, Hash $sshkey| {
    user { $client:
      ensure         => present,
      home           => "/home/${client}",
      managehome     => true,
      purge_ssh_keys => true,
    }
    ssh_authorized_key { "borgbackup-root@${client}":
      ensure  => present,
      user    => $client,
      type    => $sshkey['type'],
      options => ['restrict', 'command="borg serve --append-only"'],
      key     => $sshkey['data'],
    }
  }
}
