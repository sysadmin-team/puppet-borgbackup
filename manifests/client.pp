# @summary Configures a backup client
#
# This class deploys the required scripts and dependencies to run backup
# jobs on a node.
#
# @example
#   class { 'borgbackup::client':
#     borgpassphrase => 'supersecret',
#   }
#
# @param borgpassphrase
#   The BORG_PASSPHRASE environment variable (for more info, see:
#   https://borgbackup.readthedocs.io/en/stable/usage/general.html)
#
class borgbackup::client (
  String $borgpassphrase,
) {
  include borgbackup

  ensure_packages(['libguestfs-tools','moreutils'])

  ['runbackuplv.sh', 'runbackupfs.sh'].each |String $script| {
    file { "/usr/local/sbin/${script}":
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      source  => "puppet:///modules/borgbackup/${script}",
      require => File['/etc/borgbackup/secret'],
    }
  }

  file { '/etc/borgbackup':
    ensure  => directory,
    owner   => 'root',
    group   => 'root',
    mode    => '0700',
    require => Package['borgbackup'],
  }

  file { '/etc/borgbackup/secret':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    content => $borgpassphrase,
    require => File['/etc/borgbackup'],
  }
}
