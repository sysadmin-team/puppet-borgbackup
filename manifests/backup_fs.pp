# @summary Sets up a weekly backup job for a filesystem
#
# Creates a systemd timer that calls the backup script for backing up
# a filesystem. Defaults to backing up everything, this can be adjusted
# by setting the mountpoint parameter.
#
# @example
#   borgbackup::backup_fs { 'backstuffup':
#     excludes     => ['/var/log','/tmp'],
#     backupserver => 'backups.example.org',
#   }
#
# @param backupserver
#   The server backups are written to.
#
# @param mountpoint
#   The point in our filesystem from where we want to back up, defaults
#   to / to provide full backups.
#
# @param backupuser
#   The username on the backupserver, defaults to the client's hostname.
#
# @param basedir
#   The directory on the backupserver in which our borg repository
#   resides.
#
# @param repo
#   The name of our borg repository.
#
# @param dow
#   The day of the week in which we want the backup job to run.
#
# @param rnd
#   The random time offset (from midnight) on which the backup job is
#   run.
#
# @param excludes
#   An array of patterns for paths to exclude from backups (see:
#   https://borgbackup.readthedocs.io/en/stable/usage/create.html)
#
define borgbackup::backup_fs (
  String $backupserver,
  String $backupuser      = $trusted['hostname'],
  String $basedir         = "/home/${backupuser}",
  String $repo            = $title,
  String $dow             = 'Sun',
  String $rnd             = '24h',
  Array[String] $excludes = [],
  String $mountpoint      = '/',
) {
  include borgbackup::client

  file { "/etc/borgbackup/${repo}.excludes":
    content => epp('borgbackup/excludes.epp', { excludes => $excludes }),
    mode    => '0600',
    owner   => 'root',
    group   => 'root',
  }

  systemd::timer { "run-backup-${repo}.timer":
    timer_content   => epp('borgbackup/run-backup.timer.epp', {
        dow => $dow,
        rnd => $rnd,
    }),
    service_content => epp('borgbackup/run-backup-fs.service.epp', {
        mountpoint   => $mountpoint,
        backupuser   => $backupuser,
        backupserver => $backupserver,
        basedir      => $basedir,
        repo         => $repo,
    }),
    active          => true,
    enable          => true,
    require         => File["/etc/borgbackup/${repo}.excludes"],
  }

  exec { "/usr/bin/borg init --encryption=keyfile --append-only '${backupuser}@${backupserver}:${basedir}/${repo}'":
    onlyif      => "/usr/bin/borg list '${backupuser}@${backupserver}:${basedir}/${repo}' 2>&1 |grep -qs 'does not exist'",
    environment => "BORG_PASSPHRASE=${$borgbackup::client::borgpassphrase}",
    require     => Class['borgbackup::client'],
  }
}
